﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace WebDriverTask1;

public class Class1
{

    IWebDriver? driver;
    WebDriverWait? wait;


    [SetUp]
        public void Initialize()
        {
            driver = new ChromeDriver();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

        }
    [Test]
    public void WebDriverTest()
    {
        //Open https://pastebin.com/ or a similar service in any browser.
#pragma warning disable CS8602 // Dereference of a possibly null reference.
        driver.Navigate().GoToUrl("https://pastebin.com");
#pragma warning restore CS8602 // Dereference of a possibly null reference.
        //Accept cookie consent
#pragma warning disable CS8602 // Dereference of a possibly null reference.
        wait.Until(drv => drv.FindElement(By.Id("qc-cmp2-ui")).FindElement(By.CssSelector("button[mode='primary'][class=' css-47sehv']"))).Click();
#pragma warning restore CS8602 // Dereference of a possibly null reference.

        Thread.Sleep(2000);

        //Create 'New Paste' with the following attributes:
        //* Code: "Hello from WebDriver"
        String textContent = "Hello from WebDriver";
        String title = "helloweb";
        driver.FindElement(By.Id("postform-text")).SendKeys(textContent);
        
        //* Paste Expiration: "10 Minutes"
        //Find element
        wait.Until(drv => drv.FindElement(By.Id("postform-expiration")));
        var dropdown = driver.FindElement(By.Id("select2-postform-expiration-container"));
        
        //Scroll into view
        Actions actions = new Actions(driver);
        actions.MoveToElement(dropdown);
        actions.Perform();

        //Select the value
        dropdown.Click();        
        var dropdownValue = driver.FindElement(By.Id("select2-postform-expiration-results")).FindElement(By.XPath("//li[contains(text(),'10 Minutes')]"));
        dropdownValue.Click();

        //* Paste Name / Title: "helloweb"
        //var pasteTitle = driver.FindElement(By.Id("postform-name"));
        driver.FindElement(By.Id("postform-name")).SendKeys(title);
        //actions.MoveToElement(pasteTitle);
        //actions.Perform();
        //pasteTitle.SendKeys(title);


        //Click - Create new paste
        //var submitButton = driver.FindElement(By.CssSelector("button[type='submit'][class='btn -big']"));
        driver.FindElement(By.CssSelector("button[type='submit'][class='btn -big']")).Click();
        //actions.MoveToElement(submitButton);
        //actions.Perform();
        //submitButton.Click();

        //Wait for new paste to be created
        //wait.Until(drv => drv.Title.Equals(title+" - Pastebin.com"));
    }

    [TearDown]
    public void End()
    {
#pragma warning disable CS8602 // Dereference of a possibly null reference.
        driver.Quit();
#pragma warning restore CS8602 // Dereference of a possibly null reference.
    }
}
